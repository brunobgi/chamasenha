$(document).ready(function(){

    $('.input').focus(function(){
      $(this).parent().find(".label-txt").addClass('label-active');
    });
  
    $(".input").focusout(function(){
      if ($(this).val() == '') {
        $(this).parent().find(".label-txt").removeClass('label-active');
      };
    });

    $('#password').keypress(function(){
        var key = (window.event)?event.keyCode:e.which;
        if(!(key > 47 && key < 58 || key == 13)) {
            return false;					
        }
    });
    $('#callPassword').submit(function(e){
        e.preventDefault();
        var password = $('#password').val();
        if(password != ''){
            responsiveVoice.speak(password, "Brazilian Portuguese Female");
            // $('#password').val('');
        }
        else{
            alert('PREENCHA A SENHA');
            return false;
        }
    });
  
  });